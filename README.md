# Week 1

Agenda:

Class' first meeting with Dr. Salah, Aerospace Design Project lecturer, and Mr. Fiqri, PhD Student. Introduction and explanation were given to ensure that students are informed about this project.

Students are divided into a few subsystems so that this project can run successfully as envisioned. There are 6 subsystems:

1. Design and Structure
2. Propulsion and Power
3. Flight System Integration
4. Software and Control System
5. Simulation
6. Payload Design (Crop Sprayer)

A flowchart has been created to provide an overview of developing the airship:

![flowchart](/uploads/a92b6937ea61f221adccba955e160803/WhatsApp_Image_2021-11-01_at_10.19.26.jpeg)
